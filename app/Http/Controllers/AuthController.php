<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Tymon\JWTAuth\JWTAuth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts;

class AuthController extends Controller
{

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Usuario o contaseña invalidos'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function tokenGoogle(){
        $token = request('token');
        try {
            $user = Socialite::driver('google')->userFromToken($token);
        }catch(\Exception $e){
            return response()->json(['error' => 'Usuario no encontrado en google','info' => $e->getMessage()],404);
        }

        $finduser = User::where('google_id', $user->id)->first();

        if ($finduser) {
            return $this->respondWithToken(Auth::login($finduser));
        } else {
            User::create([
                'nombre' => $user->name,
                'email' => $user->email,
                'google_id' => $user->id,
                'imagen' => $user->avatar
            ]);
            $finduserNew = User::where('google_id', $user->id)->firstOrFail();
            return $this->respondWithToken(Auth::login($finduserNew));
        }
    }

    public function validar(){
        return response()->json(['Estado Token' => 'Token valido']);
    }

    public function me()
    {
        $user = auth()->user();

        if ($user->google_id==NULL){
            $informacion = ['id' => $user['id'] ,
                'nombre' => $user['nombre'],
                'apellido_paterno' => $user['apellido_paterno'],
                'apellido_materno' => $user['apellido_materno'],
                'telefono' => $user['telefono'],
                'sucursal_id' => $user['sucursal_id'],
                'email' => $user['email'],
            ];
        }else{
            $informacion = ['id' => $user['id'] ,
                'nombre' => $user['nombre'],
                'email' => $user['email'],
                'imagen' => $user['imagen']
            ];
        }
        return response()->json($informacion);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Fin de sesion exitoso']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 1
        ]);
    }

}
