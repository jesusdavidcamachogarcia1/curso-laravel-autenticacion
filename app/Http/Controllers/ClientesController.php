<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientes;
use App\Http\Requests\UpdateCliente;
use App\Models\Cliente;
use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ClientesController extends Controller
{

    public function index(Request $request)
    {
        $tables = $request->input('tables');
        $filtro = $request->input('filtro');
        $columna = $request->input('column');
        $ordenamiento = $request->input('order');

        $selectGeneral = DB::raw("clientes.id ,clientes.denominacion , clientes.requiere_factura , clientes.email");
        $selectAsesores = DB::raw("clientes.asesor_id , u.nombre as nombre_usuario , u.apellido_paterno , u.apellido_materno , u.telefono");
        $selectSucursales = DB::raw("clientes.sucursal_id , s.nombre , s.calle , s.num_ext , s.colonia , s.cp , s.telefono , s.gerente , s.encargado");

        if ($tables == 'clientes'){
            $clientes = Cliente::select($selectGeneral,'asesor_id','sucursal_id');
        }elseif ($tables == 'asesores'){
            $clientes = Cliente::join('users as u','clientes.asesor_id','=','u.id')->select($selectGeneral,$selectAsesores);
        }elseif ($tables == 'sucursales'){
            $clientes = Cliente::join('sucursales as s','clientes.sucursal_id','=','s.id')->select($selectGeneral,$selectSucursales);
        }else{
           $clientes = Cliente::join('users as u','clientes.asesor_id','=','u.id')->join('sucursales as s','clientes.sucursal_id','=','s.id')
               ->select($selectGeneral,$selectAsesores,$selectSucursales);
        }

        if ($filtro == 'all'){
            $clientes = $clientes->withTrashed();
        }elseif ($filtro == 'deleted'){
            $clientes = $clientes->onlyTrashed();
        }

        if (Schema::hasColumn('clientes',$columna)){
            if ($ordenamiento == 'desc'){
                $clientes = $clientes->orderBy($columna,$ordenamiento);
            }
            $clientes = $clientes->orderBy($columna);
        }

        $clientes = $clientes->get();

        return response()->json($clientes);
    }

    public function store(StoreClientes $request)
    {
        $data = $request->all();

        Cliente::create($data);

        return response()->json(['result'=>'ok']);

    }

    public function show($id)
    {
        $cliente = Cliente::select('denominacion', 'asesor_id', 'sucursal_id', 'requiere_factura', 'email')
            ->where('id',$id)->firstOrFail();

        return response()->json($cliente);
    }

    public function update(UpdateCliente $request, $id)
    {
        $data = $request->all();
        $cliente = Cliente::where('id',$id)->firstOrFail();

        $cliente->update($data);
        return response()->json(['result'=>'ok']);
    }

    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();
        return response()->json(['result'=>'ok']);
    }
}
