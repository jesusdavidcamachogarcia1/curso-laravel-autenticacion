<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Models\User;
use App\Models\Sucursal;
use Illuminate\Http\Request;

class RegistroController extends Controller
{

    public function store(StoreUser $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($data['password']);

        User::create($data);

        return response()->json(['result'=>'ok']);
    }

    public function getSucursales(){
        $sucursales = Sucursal::select('id','nombre')->get();
        return  response()->json($sucursales);
    }

}
