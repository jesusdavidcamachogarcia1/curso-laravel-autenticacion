<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSucursal;
use App\Http\Requests\UpdateSucursal;
use App\Models\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class SucursalesController extends Controller
{
    public function index(Request $request)
    {
        $sucursales = Sucursal::select('id','nombre', 'calle', 'num_ext', 'num_int', 'colonia', 'cp', 'telefono', 'gerente', 'encargado');

        $filtro = $request->input('filtro');
        $columna = $request->input('column');
        $ordenamiento = $request->input('order');

        if ($filtro == 'all'){
            $sucursales = $sucursales->withTrashed();
        }elseif ($filtro == 'deleted'){
            $sucursales = $sucursales->onlyTrashed();
        }

        if (Schema::hasColumn('sucursales',$columna)){
            if ($ordenamiento == 'desc'){
                $sucursales = $sucursales->orderBy($columna,$ordenamiento);
            }
            $sucursales->orderBy($columna);
        }
        $sucursales = $sucursales->get();

        return  response()->json($sucursales);
    }

    public function store(StoreSucursal $request)
    {
        $data = $request->all();

        Sucursal::create($data);

        return response()->json(['result'=>'ok']);
    }

    public function show($id)
    {
        $sucursal = Sucursal::select('id','nombre', 'calle', 'num_ext', 'num_int', 'colonia', 'cp', 'telefono', 'gerente', 'encargado')
            ->where('id',$id)->firstOrFail();

        return response()->json($sucursal);
    }

    public function update(UpdateSucursal $request, $id)
    {
        $data = $request->all();
        $sucursal = Sucursal::where('id',$id)->firstOrFail();

        $sucursal->update($data);
        return response()->json(['result'=>'ok']);

    }

    public function destroy($id)
    {
        $sucursal = Sucursal::findOrFail($id);
        $sucursal->delete();
        return response()->json(['result'=>'ok']);
    }
}
