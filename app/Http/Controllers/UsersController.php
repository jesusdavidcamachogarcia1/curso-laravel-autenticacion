<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class UsersController extends Controller
{
    public function index(Request $request)
    {
        $tables = $request->input('table');
        $filtro = $request->input('filtro');
        $columna = $request->input('column');
        $ordenamiento = $request->input('order');

        $consultaGeneral = DB::raw("users.id , users.nombre as nombre_usuario , users.apellido_paterno , users.apellido_materno ,
         users.telefono as telefono_usuario , users.email, users.sucursal_id");

        if($tables == 'users'){
            $usuarios = User::select($consultaGeneral);
        }elseif ($tables =='google') {
            $usuarios = User::select($consultaGeneral)->where('google_id','!=','NULL');
        }else{
            $usuarios = User::join('sucursales as s','users.sucursal_id','=','s.id')
                ->select($consultaGeneral,'s.nombre','s.calle','s.num_ext','s.colonia','s.cp','s.telefono','s.gerente','s.encargado');
        }

        if($filtro == 'all'){
            $usuarios = $usuarios->withTrashed();
        }elseif ($filtro == 'deleted'){
            $usuarios = $usuarios->onlyTrashed();
        }

        if (Schema::hasColumn('users',$columna)){
            if ($ordenamiento == 'asc' or $ordenamiento == 'desc'){
                $usuarios = $usuarios->orderBY($columna,$ordenamiento);
            }
            $usuarios->orderBy($columna);
        }

        $usuarios = $usuarios->get();

        return response()->json($usuarios);

    }

    public function store(StoreUser $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        User::create($data);
        return response()->json(['result'=>'ok']);
    }

    public function show($id,Request $request)
    {
        $filtro = $request->input('filtro');

        if($filtro =='withSucursales'){
            $usuario = User::join('sucursales as s','users.sucursal_id','=','s.id')
                ->select('users.id','users.nombre','users.apellido_paterno','users.apellido_materno','users.telefono','users.email',
                    's.id','s.nombre','s.calle','s.num_ext','s.colonia','s.cp','s.telefono','s.gerente','s.encargado')->where('users.id',$id);
        }else{
            $usuario = User::select('nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'sucursal_id', 'email')
                ->where('id',$id);
        }

        $usuario = $usuario->firstOrFail();

        return response()->json($usuario);
    }

    public function update(UpdateUser $request, $id)
    {
        $data = $request->all();

        $usuario = User::where('id',$id)->firstOrFail();

        if (array_key_exists('password',$data)) {
            $data['password'] = bcrypt($data['password']);
        }

        $usuario->update($data);

        return response()->json(['result'=>'ok']);
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        $usuario->delete();

        return response()->json(['result'=>'ok']);
    }

}
