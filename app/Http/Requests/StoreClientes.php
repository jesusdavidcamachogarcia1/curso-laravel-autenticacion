<?php

namespace App\Http\Requests;

use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class StoreClientes extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido',
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'boolean' => 'Dato invalido en :attribute',
            'unique' => 'El email ya esta en uso con otro cliente',
            'integer' => 'El campo :attribute tiene que ser un numero entero',
            'exists' => 'llave foranea de :attribute inexistente'
        ];
    }

    public function rules()
    {
        return [
            'denominacion' => 'required|max:255',
            'asesor_id' => ['required','integer',Rule::exists(User::class,'id')->where('deleted_at','NULL')],
            'sucursal_id' => ['required','integer',Rule::exists(Sucursal::class,'id')->where('deleted_at','NULL')],
            'requiere_factura' => 'required|boolean',
            'email' => 'required|string|email|max:255|unique:clientes'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json($errors,422)
        );

    }
}
