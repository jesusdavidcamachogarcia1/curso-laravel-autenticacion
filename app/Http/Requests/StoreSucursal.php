<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class StoreSucursal extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido',
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'regex' => 'El campo :attribute solo puede contener letras',
            'digits' => 'El campo :attribute no es valido',
            'cp.digits' => 'El campo del codigo postal no es valido debe contener 5 numeros',
            'telefono.digits' => 'El numero de telefono no es valido debe contener 10 numeros',
            'integer' => 'El campo :attribute solo admite numeros enteros'
        ];
    }

    public function rules()
    {
        return [
            'nombre' => 'required|max:255',
            'calle' => 'required|max:255',
            'num_ext' => 'required|integer',
            'num_int' => 'integer',
            'colonia' => 'required|max:255',
            'cp' => 'required|digits:5',
            'telefono' => 'required|digits:10',
            'gerente'  => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'encargado' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json($errors,422)
        );

    }
}


