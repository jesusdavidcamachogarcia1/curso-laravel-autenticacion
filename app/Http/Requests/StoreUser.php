<?php

namespace App\Http\Requests;

use App\Models\Sucursal;
use http\Env\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rules\Password;

class StoreUser extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido',
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'regex' => 'El campo :attribute solo puede contener letras',
            'digits' => 'El campo :attribute no es valido',
            'email' => 'El campo :attribute no es un correo valido',
            'unique' => 'El dato del campo :attribute ya existe en otro usuario',
            'integer' => 'El campo :attribute solo admite numeros enteros',
            'sucursal_id.exists' => 'llave foranea de sucursal inexistente',
            'sucursal_id.unique' => 'llave foranea de sucursal inactiva',
            'password.min' => 'La contraseña debe contener al menos 6 caracteres',
            'password.max' => 'La contraseña no debe ser mayor de 20 caracteres'
        ];

    }

    public function rules()
    {

        return [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_paterno' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_materno' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'telefono' => 'required|digits:10|unique:users',
            'sucursal_id' => ['required','integer',Rule::exists(Sucursal::class,'id')->where('deleted_at','NULL')],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required','string','min:6','max:20',Password::min(1)
                ->letters()
                ->mixedcase()
                ->numbers()
                ->symbols()
            ],
            'google_id' => 'unique:users'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json($errors,422)
        );

    }


}
