<?php

namespace App\Http\Requests;

use App\Models\Sucursal;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UpdateCliente extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'cp.digits' => 'El campo del codigo postal no es valido debe contener 5 numeros',
            'boolean' => 'Dato invalido en :attribute',
            'unique' => 'El email ya esta en uso con otro cliente',
            'exists' => 'llave foranea de :attribute inexistente'
        ];
    }

    public function rules()
    {
        return [
            'denominacion' => 'max:255',
            'asesor_id' => ['integer',Rule::exists(User::class,'id')->where('deleted_at','NULL')],
            'sucursal_id' => ['integer',Rule::exists(Sucursal::class,'id')->where('deleted_at','NULL')],
            'requiere_factura' => 'boolean',
            'email' => 'string|email|max:255|exclude|unique:clientes',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json($errors,422)
        );

    }
}
