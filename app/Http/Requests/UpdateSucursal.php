<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class UpdateSucursal extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'regex' => 'El campo :attribute solo puede contener letras',
            'digits' => 'El campo :attribute no es valido',
            'cp.digits' => 'El campo del codigo postal no es valido debe contener 5 numeros',
            'integer' => 'El campo :attribute solo admite numeros enteros'
        ];
    }

    public function rules()
    {
        return [
            'nombre' => 'max:255',
            'calle' => 'max:255',
            'num_ext' => 'integer',
            'num_int' => 'integer|nullable',
            'colonia' => 'max:255',
            'cp' => 'integer|digits:5',
            'telefono' => 'digits:10',
            'gerente'  => 'regex:/^[\pL\s\-]+$/u|max:255',
            'encargado' => 'regex:/^[\pL\s\-]+$/u|max:255'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json($errors,422)
        );
    }
}
