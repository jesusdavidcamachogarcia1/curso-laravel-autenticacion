<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;
use App\Models\Sucursal;

class UpdateUser extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'max' => 'El campo :attribute esta exediendo la cantidad de caracteres',
            'regex' => 'El campo :attribute solo puede contener letras',
            'digits' => 'El campo :attribute no es valido',
            'email' => 'El campo :attribute no es un correo valido',
            'unique' => 'El dato del campo :attribute ya existe en otro usuario',
            'integer' => 'El campo :attribute solo admite numeros enteros',
            'sucursal_id.exists' => 'llave foranea de sucursal inexistente',
            'sucursal_id.unique' => 'llave foranea de sucursal inactiva',
            'password.min' => 'La contraseña debe contener al menos 6 caracteres',
            'password.max' => 'La contraseña no debe ser mayor de 20 caracteres'
        ];

    }

    public function rules()
    {
        return [
            'nombre' => 'regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_paterno' => 'regex:/^[\pL\s\-]+$/u|max:255',
            'apellido_materno' => 'regex:/^[\pL\s\-]+$/u|max:255',
            'telefono' => 'digits:10',
            'sucursal_id' => ['integer',Rule::exists(Sucursal::class,'id')->where('deleted_at','NULL')],
            'email' => 'string|email|max:255|exclude|unique:users',
            'password' => ['string','min:6','max:20',Password::min(1)
                ->letters()
                ->mixedcase()
                ->numbers()
                ->symbols()
            ],
            'google_id' => 'unique:users'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json($errors,422)
        );

    }
}
