<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserChangeStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name','nombre');
            $table->string('apellido_paterno')->after('name');
            $table->string('apellido_materno')->after('apellido_paterno');
            $table->string('telefono',10)->after('apellido_materno');
            $table->unsignedBigInteger('sucursal_id')->after('telefono');
            $table->softDeletes();

            $table->foreign('sucursal_id')->references('id')->on('sucursales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('nombre','name');
            $table->dropForeign('sucursal_id');
            $table->removeColumn(['apellido_paterno','apellido_materno','telefono','sucursal_id']);
            $table->dropSoftDeletes();
        });
    }
}
