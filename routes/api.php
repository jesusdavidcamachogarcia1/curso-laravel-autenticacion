<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//rutas generales con restriccion de token
Route::group(['middleware'=>'auth:admin'], function (){
    Route::apiResource('users',\App\Http\Controllers\UsersController::class);
    Route::apiResource('clientes',\App\Http\Controllers\ClientesController::class);
    Route::apiResource('sucursales', \App\Http\Controllers\SucursalesController::class);
    Route::post('logout',  [\App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('refresh',  [\App\Http\Controllers\AuthController::class, 'refresh']);
    Route::post('me',  [\App\Http\Controllers\AuthController::class, 'me']);
    Route::post('validarToken',[\App\Http\Controllers\AuthController::class,'validar']);
});

//rutas necesarias para agregar usuarios
Route::get('getSucursales', [\App\Http\Controllers\RegistroController::class, 'getSucursales']);
Route::post('addUser', [\App\Http\Controllers\RegistroController::class, 'store']);

//ruta de logeo
Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
//ruta para login con google
Route::post('longinCuentaGoogle',[\App\Http\Controllers\AuthController::class,'tokenGoogle']);
